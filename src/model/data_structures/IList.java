package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement,  , getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {

	Integer getSize();

	public void addNodeKnown(Node<T> nodeAdd, int index);
	
	public void addNodeBeginning(Node<T> nodeAdd);
	
	public void addNodeEnd(Node<T> nodeAdd);
	
	public Node<T> getElement(int index);
	
	public void deleteNodeKnown(Node<T> N);	
	
}
