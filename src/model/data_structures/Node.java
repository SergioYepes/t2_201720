package model.data_structures;

public class Node<T> {

	/**
	 * Predecesor del nodo.
	 */
	private Node<T> prior;
	
	/**
	 * Sucesor del nodo
	 */
	private Node<T> next;
	
	private T item;
	
	//CONSTRUCTOR
	/**
	 * Constructor de la clase
	 */
	public Node(T t){
		prior = null;
		next = null;
		item = t;
	}
	
	//METHODS
	
	/**
	 * Define el nodo anterior.
	 */
	public void setPrior(Node<T> N){
		prior = N;
	}
	
	/**
	 * Define el nodo siguiente
	 */
	public void setNext(Node<T> N){
		next = N;
	}
	
	/**
	 * Retorna el nodo anterior.
	 * @return Nodo anterior.
	 */
	public Node<T> getPrior(){
		return prior;
	}
	
	/**
	 * Retorna el nodo siguiente.
	 * @return Nodo siguiente.
	 */
	public Node<T> getNext(){
		return next;
	}
	
	public T getItem(){
		return item;
	}
	
	
	public void disconnect(){
		prior.setNext(next);
		next.setPrior(prior);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
