package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class DoublyLinkedList<T> implements IList<T> {

	//ATTRIBUTES
	/**
	 * Primer nodo de la lista
	 */
	private Node<T> first;

	/**
	 * �ltimo nodo de la lista
	 */
	private Node<T> last;

	private ListIterator<T> iter;

	//CONSTRUCTOR

	public DoublyLinkedList(){
		first = null;
		last = null;
		iter = new ListIterator<T>(first);
	}
	private class ListIterator<T> implements Iterator<T> {
		private Node<T> current;

		public ListIterator(Node<T> pFirst) {
			current = pFirst;
		}

		public boolean hasNext()  { return current != null;                     }
		public void remove()      { throw new UnsupportedOperationException();  }

		public T next() {
			if (!hasNext()) throw new NoSuchElementException();
			T item = current.getItem();
			current = current.getNext(); 
			return item;
		}
	}

	public Iterator<T> iterator() {
		return new ListIterator<T>(first);
	}



	public Integer getSize() {
		Integer size = 0;
		if(first != null){
			size++;
			if(first == last)
				return size;
			Node<T> actual = first;
			while(actual.getNext()!=null){
				size++;
				actual = actual.getNext();
			}
		}
		return size;
	}

	public void addNodeKnown(Node<T> nodeAdd, int index) {
		if(first == null)
			addNodeBeginning(nodeAdd);
		else if(getElement(index).equals(last)) {
			last.setNext(nodeAdd);
			nodeAdd.setPrior(last);
			last = nodeAdd;
		}
		else{
			Node<T> nodeNext = getElement(index);
			nodeAdd.setNext(nodeNext);
			nodeNext.getPrior().setNext(nodeAdd);
			nodeAdd.setPrior(nodeNext.getPrior());
			nodeNext.setPrior(nodeAdd);
		}
	}

	public void addNodeBeginning(Node<T> nodeAdd){
		if(first == null){
			first = nodeAdd;
			last = nodeAdd;
		}
		else{
			nodeAdd.setNext(first);
			first.setPrior(nodeAdd);
			first = nodeAdd;
		}
	}

	public void addNodeEnd(Node<T> nodeAdd){
		if(first==null)
			addNodeBeginning(nodeAdd);
		else{
			last.setNext(nodeAdd);
			nodeAdd.setPrior(last);
			last = nodeAdd;
		}
	}
	public void deleteNodeKnown(Node<T> nodeDel) {
		if(first != null){
			if(first.equals(nodeDel)){
				if(first.equals(last)){
					first = null;
					last = null;
				}
				else{
					first = first.getNext();
					first.setPrior(null);
				}
			}
			else if(last.equals(nodeDel)){
				last = last.getPrior();
				last.setNext(null);
			}
			else{
				nodeDel.disconnect();
			}
		}
	}

	public Node<T> getElement(int index){
		Node<T> element = null;
		int sum = 0;
		Node<T> actual = first;
		if(index == sum)
			return first;
		sum++;
		while(actual.getNext() != null && element == null){
			actual = actual.getNext();
			if(sum == index)
				element = actual;
			sum++;
			
		}	
		return element;
	}
}
