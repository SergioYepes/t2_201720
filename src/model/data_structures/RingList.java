package model.data_structures;


import java.util.Iterator;
import java.util.NoSuchElementException;

public class RingList<T> implements IList<T>{
	//ATTRIBUTES
	/**	
	 * N�mero de elementos de la lista
	 */
	int size;

	/**
	 * Primer nodo de la lista
	 */
	private Node<T> first;

	/**
	 * �ltimo nodo de la lista
	 */
	private Node<T> last;

	//CONSTRUCTOR

	public RingList(){
		size = 0;
		first = null;
		last = null;
	}



	private class ListIterator<T> implements Iterator<T> {
		private Node<T> current;

		public ListIterator(Node<T> first) {
			current = first;
		}

		public boolean hasNext()  { return current != null;                     }
		public void remove()      { throw new UnsupportedOperationException();  }

		public T next() {
			if (!hasNext()) throw new NoSuchElementException();
			T item = current.getItem();
			current = current.getNext(); 
			return item;
		}
	}

	public Iterator<T> iterator() {
		return new ListIterator<T>(first);
	}
	public Integer getSize() {
		return size;
	}

	public void addNodeKnown(Node<T> nodeAdd, int index) {
		
		Node<T> actual = first;
		if(index == size-1)
			addNodeEnd(nodeAdd);
		else if(index == 0)
			addNodeBeginning(nodeAdd);
		else {
			Node<T> nodeNext = getElement(index);
			Node<T> nodePrior = nodeNext.getPrior();
			nodeAdd.setNext(nodeNext);
			nodePrior.setNext(nodeAdd);
			nodeAdd.setPrior(nodePrior);
			nodeNext.setPrior(nodeAdd);
			size++;
		}
	}

	public void addNodeBeginning(Node<T> nodeAdd){
		if(first == null){
			first = nodeAdd;
			last = nodeAdd;
			first.setPrior(last);
			last.setNext(first);
		}
		else{
			nodeAdd.setNext(first);
			first.setPrior(nodeAdd);
			first = nodeAdd;
			first.setPrior(last);
			last.setNext(first);
		}
		size++;
	}

	public void addNodeEnd(Node<T> nodeAdd){
		if(first == null){
			addNodeBeginning(nodeAdd);
		}
		else{
			last.setNext(nodeAdd);
			nodeAdd.setPrior(last);
			last = nodeAdd;
			last.setNext(first);
			first.setPrior(last);
			size++;	
		}
	}
	public void deleteNodeKnown(Node<T> nodeDel) {
		if(first != null){
			if(first.equals(nodeDel)){
				if(first.equals(last)){
					first = null;
					last = null;
				}
				else{
					first = first.getNext();
					first.setPrior(last);
					last.setNext(first);
				}
			}
			else if(last.equals(nodeDel)){
				last = last.getPrior();
				last.setNext(first);
				first.setPrior(last);
			}
			else{
				nodeDel.disconnect();
			}
			size--;
		}
	}

	public Node<T> getElement(int index){
		Node<T> element = null;
		if(size!=0) {
			Node<T> actual = first;
			int temp = 0;
			if(0==(index%size))
				element = first;
			while(element == null) {			
				if(temp == index%size)
					element = actual;
				actual = actual.getNext();
				temp++;
			}
		}
		return element;
	}
}
