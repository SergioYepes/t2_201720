package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import api.ISTSManager;
import model.vo.Route;
import model.vo.Stop;
import model.vo.StopTimes;
import model.vo.Trip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IList;
import model.data_structures.Node;
import model.data_structures.RingList;

public class STSManager implements ISTSManager {


	private DoublyLinkedList<StopTimes> stopTList = new DoublyLinkedList<StopTimes>();

	private RingList<Trip> tripList = new RingList<>();

	private DoublyLinkedList<Route> routeList = new DoublyLinkedList<>();

	private RingList<Stop> stopList = new RingList<>();

	@Override
	public void loadRoutes(String routesFile) {
		// TODO Auto-generated method stub
		File archivo = new File(routesFile);
		if(archivo.exists()) {
			try {
				BufferedReader reader = new BufferedReader(new FileReader(archivo));
				String line = reader.readLine();

				line = reader.readLine();
				while(line != null) {
					String[] info = line.split(",");
					Route route = new Route(Integer.parseInt(info[0].trim()),info[1].trim(),info[2].trim(),info[3].trim(),info[4].trim(),Integer.parseInt(info[5].trim()),info[6].trim(),info[7].trim(),info[8].trim());
					routeList.addNodeEnd(new Node<Route>(route));
					line = reader.readLine();
				}
				reader.close();
			}
			catch(Exception e) {
				System.out.println("Error en loadRoutes()\n");
				System.out.println("------------------------------------\n");
				System.out.println("------------------------------------\n");
				System.out.println("------------------------------------\n");
				System.out.println("------------------------------------\n");

			}

		}
	}

	@Override
	public void loadTrips(String tripsFile) {
		// TODO Auto-generated method stub
		File archivo = new File(tripsFile);
		if(archivo.exists()) {
			try {
				BufferedReader reader = new BufferedReader(new FileReader(archivo));
				String line = reader.readLine();
				line = reader.readLine();
				while(line != null) {
					String[] info = line.split(",");
					boolean wheelsAllowed = (Integer.parseInt(info[8].trim()) == 0)?false:true;
					boolean bikesAllowed = (Integer.parseInt(info[9].trim())==0)?false:true;
					int tripId = Integer.parseInt(info[2].trim());
					Trip trip = new Trip(Integer.parseInt(info[0].trim()),Integer.parseInt(info[1].trim()),tripId,info[3].trim(),info[4].trim(),Integer.parseInt(info[5].trim()),Integer.parseInt(info[6].trim()),Integer.parseInt(info[7].trim()),wheelsAllowed,bikesAllowed);
					Node<Trip> add = new Node<>(trip);
					if(tripList.getElement(0) != null){
						int position = 0;
						Node<Trip> current= tripList.getElement(0);
						while(current.getNext()!=tripList.getElement(0) && tripId>current.getItem().getId()) {
							current = current.getNext();
							position++;
						}
						tripList.addNodeKnown(add, position);
					}
					else{
						tripList.addNodeBeginning(add);
					}
					line = reader.readLine();
				}
				reader.close();
			}
			catch(Exception e) {
				System.out.println("Error en loadTrips()\n");
				System.out.println("------------------------------------\n");
				System.out.println("------------------------------------\n");
				System.out.println("------------------------------------\n");
			}

		}
	}

	@Override
	public void loadStopTimes(String stopTimesFile) {
		// TODO Auto-generated method stub
		File archivo = new File(stopTimesFile);
		if(archivo.exists()) {
			try {
				BufferedReader reader = new BufferedReader(new FileReader(archivo));
				String line = reader.readLine();

				line = reader.readLine();
				while(line != null) {
					String[] info = line.split(",");
					String trip_id = info[0].trim();
					String arrival_time = info[1].trim();
					String departure_time = info[2].trim();
					String stop_id = info[3].trim();
					String stop_sequence = info[4].trim();
					String stop_headsign = info[5].trim();
					String pickup_type = info[6].trim();
					String drop_off_type = info[7].trim();
					String shape_dist_travelled = (info.length==7)?"":info[7];

					int intTripId = (trip_id.equals(""))?-1:Integer.parseInt(trip_id);
					int intStopId = (stop_id.equals(""))?-1:Integer.parseInt(stop_id);
					int intStopSequence = (stop_sequence.equals(""))?-1:Integer.parseInt(stop_sequence);
					int intPick = (pickup_type.equals(""))?-1:Integer.parseInt(pickup_type);
					int intDrop = (drop_off_type.equals(""))?-1:Integer.parseInt(drop_off_type);
					double doubleShape = (shape_dist_travelled.equals(""))?-1:Double.parseDouble(shape_dist_travelled);

					StopTimes stopTimes = new StopTimes(intTripId,arrival_time,departure_time,intStopId,intStopSequence,stop_headsign,intPick,intDrop,doubleShape);
					stopTList.addNodeEnd(new Node<StopTimes>(stopTimes));
					line = reader.readLine();
				}

				reader.close();
			}
			catch(Exception e) {
				System.out.println("Error en loadStops()\n");
				System.out.println("------------------------------------\n");
				System.out.println("------------------------------------\n");
				System.out.println("------------------------------------\n");
			}

		}
	}

	@Override
	public void loadStops(String stopsFile) {
		// TODO Auto-generated method stub
		File archivo = new File(stopsFile);;
		if(archivo.exists()){
			try {
				BufferedReader reader = new BufferedReader(new FileReader(archivo));
				String line = reader.readLine();
				line = reader.readLine();
				
				while(line != null){
					String[] info = line.split(",");
					String stop_id = info[0].trim();
					String stop_code = info[1].trim();
					String stop_name = info[2].trim();
					String stop_desc = info[3].trim();
					String stop_lat = info[4].trim();
					String stop_lon = info[5].trim();
					String zone_id = info[6].trim();
					String stop_url = info[7].trim();
					String location_type = info[8].trim();
					String parent_station = (info.length==9)?"":info[9];

					int intStopId = (stop_id.equals(""))?0:Integer.parseInt(stop_id);
					int intStopCode = (stop_code.equals(""))?0:Integer.parseInt(stop_code);
					double doubleStopLat = (stop_lat.equals(""))?0:Double.parseDouble(stop_lat);
					double doubleStopLon = (stop_lon.equals(""))?0:Double.parseDouble(stop_lon);
					int intLocationType = (location_type.equals(""))?0:Integer.parseInt(location_type);

					Stop stop = new Stop(intStopId,intStopCode,stop_name,stop_desc,doubleStopLat,doubleStopLon,zone_id,stop_url,intLocationType,parent_station);
					Node<Stop> add = new Node<>(stop);
					if(stopList.getElement(0)!= null){
						int position = 0;
						Node<Stop> current= stopList.getElement(0);
						while(current.getNext()!=stopList.getElement(0) && intStopId>current.getItem().getId()) {
							current = current.getNext();
							position++;
						}
						stopList.addNodeKnown(add, position);
					}
					else{
						stopList.addNodeBeginning(add);
					}
					line = reader.readLine();
				}
				reader.close();
			}
			catch(Exception e) {
				System.out.println("Error en loadStops()\n");
				System.out.println("------------------------------------\n");
				System.out.println("------------------------------------\n");
				System.out.println("------------------------------------\n");

			}

		}
	}

	@Override
	public IList<Route> routeAtStop(String stopName) {
		// TODO Auto-generated method stub
		Node<Stop> currentStop = stopList.getElement(0);
		Node<StopTimes> currentStopT = stopTList.getElement(0);
		Node<Trip> currentTrip = tripList.getElement(0);
		Node<Route> currentRoute = routeList.getElement(0);
		int stopId = 0;
		int tripId = 0;
		int routeId = 0;
		IList<Route> ans = new DoublyLinkedList<Route>();

		while(currentStop.getNext()!=stopList.getElement(0) && stopId==0) {
			if(currentStop.getItem().getName().equals(stopName)) {
				stopId = currentStop.getItem().getId();
			}
			currentStop = currentStop.getNext();
		}
		while(currentStopT.getNext()!= null && tripId==0) {
			if(currentStopT.getItem().getStopId()==stopId) {
				tripId = currentStopT.getItem().getTripId();
			}
			currentStopT = currentStopT.getNext();
		}
		while(currentTrip.getNext()!=tripList.getElement(0)&&routeId==0) {
			if(currentTrip.getItem().getId()==tripId) {
				routeId= currentTrip.getItem().getRouteId();
			}
			currentTrip = currentTrip.getNext();
		}

		while(currentRoute.getNext()!= null) {
			if(currentRoute.getItem().getId()==routeId) {
				Route itemAdd = currentRoute.getItem();
				Node<Route> nodeAdd = new Node<Route>(itemAdd);
				ans.addNodeBeginning(nodeAdd);
			}
			currentRoute = currentRoute.getNext();
		}
		return ans;
	}

	public Stop searchStop(int id) {
		Stop ans = null;
		Node<Stop> current = stopList.getElement(0);
		while(current.getNext()!=null) {
			if(current.getItem().getId()==id) {
				ans = current.getItem();
			}
			current = current.getNext();
		}
		return ans;
	}



	@Override
	public IList<Stop> stopsRoute(String routeName, String direction) {
		// TODO Auto-generated method stub
		Node<StopTimes> currentStopT = stopTList.getElement(0);
		//Node<StopTimes> current22 = stopTList.getElement(1);
		Node<Trip> currentTrip = tripList.getElement(0);
		Node<Route> currentRoute = routeList.getElement(0);
		int tripId = 0;
		int routeId = 0;
		boolean done = false;
		IList<Stop> ans = new DoublyLinkedList<>();
		while(currentRoute.getNext()!=null && !done) {
			if(currentRoute.getItem().getShortName().equals(routeName)) {
				routeId = currentRoute.getItem().getId();
				done = true;
			}
			currentRoute = currentRoute.getNext();
		}
		done = false;
		while(currentTrip.getNext()!=tripList.getElement(0) && !done) {
			if(currentTrip.getItem().getDirectionId()==Integer.parseInt(direction)&&currentTrip.getItem().getId()==routeId) {
				tripId = currentTrip.getItem().getId();
				done = true;
			}
			currentTrip = currentTrip.getNext();
		}
		while(currentStopT.getNext()!=null){
			if(currentStopT.getItem().getTripId() == tripId){
				ans.addNodeEnd(new Node<Stop>(searchStop(currentStopT.getItem().getStopId())));
			}
			currentStopT = currentStopT.getNext();
		}
		return ans;

	}

}
