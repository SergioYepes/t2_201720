package model.vo;

public class Stop {

	/**
	 * Id de la parada.
	 */
	private int id;
	
	/**
	 * C�digo de la parada.
	 */
	private int code;
	
	/**
	 * Nombre de la parada-
	 */
	private String name;
	
	/**
	 *stop_desc 
	 */
	private String stop_desc;
	
	/**
	 * Latitud de la parada.
	 */
	private double stop_lat;
	
	/**
	 * Longitud de la parada.
	 */
	private double stop_long;
	
	/**
	 * Id de la zona.
	 */
	private String zone_id;
	
	/**
	 * Url de la parada.
	 */
	private String url;
	
	/**
	 * Tipo de locaci�n.
	 */
	private int locationType; 
	
	/**
	 * Estaci�n primaria.
	 */
	private String parent_station;
	
	//CONSTRUCTOR
	
	public Stop(int pId, int pCode, String pName, String pStopDesc, double pStopLat, double pStopLong, String pZoneId, String pUrl, int pLocationType, String pParentStation) {
		id = pId;
		code = pCode;
		name = pName;
		stop_desc = pStopDesc;
		stop_lat = pStopLat;
		stop_long = pStopLong;
		zone_id = pZoneId;
		url = pUrl;
		locationType = pLocationType;
		parent_station = pParentStation;
	}
	
	//METHODS
	
	public int getId() {
		return id;
	}
	
	public int getCode() {
		return code;
	}
	
	public String getName() {
		return name;
	}
	
	public String getStopDesc() {
		return stop_desc;
	}
	
	public double getStopLat() {
		return stop_lat;
	}
	
	public double getStopLong() {
		return stop_long;
	}
	
	public String getZoneId() {
		return zone_id;
	}
	
	public String getUrl() {
		return url;
	}
	
	public int getLocationType() {
		return locationType;
	}
	
	public String getParentStation() {
		return parent_station;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
