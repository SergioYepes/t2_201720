package model.vo;

public class StopTimes {

	/**
	 * Id del viaje en el horario de la parada.
	 */
	private int trip_id;
	
	/**
	 * Tiempo de llegada.
	 */
	private String arrivalTime;
	
	/**
	 * Tiempo de salida.
	 */
	private String departureTime;
	
	/**
	 * Id de la parada
	 */
	private int stop_id;
	
	/**
	 * Secuencia de la parada.
	 */
	private int stopSequence;
	
	/**
	 * Aviso de la parada.
	 */
	private String stopHeadSign;
	
	/**
	 * Tipo de recogida.
	 */
	private int pickUpType;
	
	/**
	 * Tipo de sacada.
	 */
	private int dropOffType;
	
	/**
	 * shape_dis.t_traveled
	 */
	private double shape_dist_traveled;
	
	//CONSTRUCTOR
	
	public StopTimes(int pTripId, String pArrivalTime,String pDepartureTime, int pStopId, int pStopSequence, String pStopHeadSign, int pPickUpType, int pDropOffType, double pShape_dist_traveled) {
		trip_id = pTripId;
		arrivalTime = pArrivalTime;
		departureTime = pDepartureTime;
		stop_id = pStopId;
		stopSequence = pStopSequence;
		stopHeadSign = pStopHeadSign;
		pickUpType = pPickUpType;
		dropOffType = pDropOffType;
		shape_dist_traveled = pShape_dist_traveled;
	}
	
	public int getTripId() {
		return trip_id;
	}
	
	public String getArrivalTime() {
		return arrivalTime;
	}
	
	public String getDepartureTime() {
		return departureTime;
	}
	
	public int getStopId() {
		return stop_id;
	}
	
	public int getStopSequence() {
		return stopSequence;
	}
	
	public String getStopHeadSign() {
		return stopHeadSign;
	}
	
	public int getPickUpType() {
		return pickUpType;
	}
	
	public int getDropOffType() {
		return dropOffType;
	}
	
	public double getShapeDistTraveled() {
		return shape_dist_traveled;
	}
}
