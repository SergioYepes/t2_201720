package model.vo;

public class Route {

	/**
	 * ID de la ruta.
	 */
	private int id;
	
	/**
	 * Nombre de la agencia de la ruta.
	 */
	private String agency;
	
	/**
	 * Nombre corto de la ruta.
	 */
	private String shortName;
	
	/**
	 * Nombre largo de la lista.
	 */
	private String longName;
	
	/**
	 * route_desc.
	 */
	private String route_desc;
	
	/**
	 * Tipo de ruta.
	 */
	private int type;
	
	/**
	 * Url de la ruta.
	 */
	private String url;
	
	/**
	 * Color de la ruta.
	 */
	private String color;
	
	/**
	 * Color del texto de la ruta
	 */
	private String colorText;
	
	//CONSTRUCTOR
	
	/**
	 * Se crea una nueva ruta.
	 * @param pId id de la ruta
	 * @param pAgency nombre de la agencia de la ruta.
	 * @param pShortName nombre corto de la ruta.
	 * @param pLongName nombre largo de la ruta.
	 * @param pRouteDesc route_desc
	 * @param pType tipo de la ruta.
	 * @param pUrl url de la ruta.
	 * @param pColor color de la ruta.
	 * @param pColorText color del texto de la ruta.
	 */
	public Route(int pId, String pAgency, String pShortName, String pLongName, String pRouteDesc, int pType, String pUrl, String pColor, String pColorText) {
		id = pId;
		agency = pAgency;
		shortName = pShortName;
		longName = pLongName;
		route_desc = pRouteDesc;
		type = pType;
		url = pUrl;
		color = pColor;
		colorText = pColorText;
	}
	
	//METHODS
	
	public int getId() {
		return id;
	}
	
	public String getAgency() {
		return agency;
	}
	
	public String getShortName() {
		return shortName;
	}
	
	public String getLongName() {
		return longName;
	}
	
	public String getRouteDesc() {
		return route_desc;
	}
	
	public int getType() {
		return type;
	}
	
	public String getUrl() {
		return url;
	}
	
	public String getColor() {
		return color;
	}
	
	public String getColorText() {
		return colorText;
	}	
}
