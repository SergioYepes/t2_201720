package model.vo;

public class Trip {

	/**
	 * Id de la ruta.
	 */
	private int route_id;
	
	/**
	 * Id del servicio.
	 */
	private int service_id;
	
	/**
	 * Id del viaje.
	 */
	private int id;
	
	/**
	 * Aviso del viaje.
	 */
	private String headsign;
	
	/**
	 * Nombre corto del viaje.
	 */
	private String shortName;
	
	/**
	 * Id de la direcci�n.
	 */
	private int direction_id;
	
	/**
	 * Id del bloque del viaje.
	 */
	private int block_id;
	
	/**
	 * Id de la forma.
	 */
	private int shape_id;
	
	/**
	 * Permiso para las sillas de ruedas en el viaje.
	 */
	private boolean wheelchairsAllowed;
	
	/**
	 * Perimos para las bicicletas en el viaje.
	 */
	private boolean bikesAllowed;
	
	//CONSTRUCTOR
	
	public Trip(int pRoutId, int pServiceId, int pId, String pHeadsign, String pShortName, int pDirectionId, int pBlockId, int pShapeId, boolean pWheelchairsAllowed, boolean pBikesAllowed) {
		route_id = pRoutId;
		service_id = pServiceId;
		id = pId;
		headsign = pHeadsign;
		shortName = pShortName;
		direction_id = pDirectionId;
		block_id = pBlockId;
		shape_id = pShapeId;
		wheelchairsAllowed = pWheelchairsAllowed;
		bikesAllowed = pBikesAllowed;
	}
	
	//METHODS
	
	public int getRouteId() {
		return route_id;
	}
	
	public int getServiceId() {
		return service_id;
	}
	
	public int getId() {
		return id;
	}
	
	public String getHeadsign() {
		return headsign;
	}
	
	public String getShortName() {
		return shortName;
	}
	
	public int getDirectionId() {
		return direction_id;
	}
	
	public int getBlockId() {
		return block_id;
	}
	
	public int getShapeId() {
		return shape_id;
	}
	
	public boolean AreWheelchairsAllowed() {
		return wheelchairsAllowed;
	}
	
	public boolean AreBikesAllowed() {
		return bikesAllowed;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
