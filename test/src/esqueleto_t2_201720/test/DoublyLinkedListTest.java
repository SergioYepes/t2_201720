package esqueleto_t2_201720.test;
import junit.framework.TestCase;

import model.data_structures.DoublyLinkedList;
import model.data_structures.Node;
public class DoublyLinkedListTest extends TestCase{
	//ATTRIBUTES
	
		DoublyLinkedList<Integer> doublyLinkedListTest;
		Node<Integer> nodeFirst;
		Node<Integer> nodeLast;
		
		private void setupEscenario1(){
			try{
				doublyLinkedListTest = new DoublyLinkedList<Integer>();
			}
			catch(Exception e){
				fail("No debi� generar error al crear la lista");
			}
		}
		
		private void setupEscenario2(){
			try{
			
				doublyLinkedListTest = new DoublyLinkedList<Integer>();
				nodeLast = new Node<>(new Integer(1));
				doublyLinkedListTest.addNodeBeginning(nodeLast);
				Node<Integer> node1 = new Node<>(new Integer(1));
				doublyLinkedListTest.addNodeBeginning(node1);
				Node<Integer> node2 = new Node<>(new Integer(1));
				doublyLinkedListTest.addNodeBeginning(node2);
				nodeFirst = new Node<>(new Integer(1));
				doublyLinkedListTest.addNodeBeginning(nodeFirst);
			}
			catch(Exception e){
				fail("No debi� generar error al crear la lista");
			}
		}
		
		public void testDoublyLinkedList(){
			setupEscenario1();
			assertEquals("Al inicio, no debe haber ning�n elemento",null,doublyLinkedListTest.getElement(0));
		}
		
		public void testGetSize(){
			setupEscenario2();
			assertEquals("Debe tener cuatro nodos",new Integer(4),doublyLinkedListTest.getSize());
		}
		
		public void testAddNodeKnown(){
			setupEscenario2();
			Node<Integer> node1 = new Node<>(new Integer(1));
			doublyLinkedListTest.addNodeKnown(node1,3);
			assertEquals("El nodo siguiente deber�a ser 'node'",nodeLast,node1.getPrior());
		}
		
		public void testAddNodeBeginning(){
			setupEscenario2();
			Node<Integer> node1 = new Node<>(new Integer(1));
			doublyLinkedListTest.addNodeBeginning(node1);
			assertEquals("El nodo anterior debe ser null",null,node1.getPrior());
			assertEquals("El nodo siguiente deber�a ser 'node'",nodeFirst,node1.getNext());
		}
		
		public void testAddNodeEnd(){
			setupEscenario2();
			Node<Integer> node1 = new Node<>(new Integer(1));
			doublyLinkedListTest.addNodeEnd(node1);
			assertEquals("El nodo siguiente debe ser null",null,node1.getNext());
			assertEquals("El nodo anterior deber�a ser 'nodeLast'",nodeLast,node1.getPrior());
		}
		
		public void testDeleteNodeKnown(){
			setupEscenario2();
			Integer size = doublyLinkedListTest.getSize();
			doublyLinkedListTest.deleteNodeKnown(nodeLast);
			assertEquals("El tama�o de la lista deber�a haber disminu�do",new Integer(size-1),doublyLinkedListTest.getSize());
		}
		public void testGetElement(){
			setupEscenario2();
			assertEquals("El ultimo nodo debe ser nodeLast",nodeLast,doublyLinkedListTest.getElement(3));
		}
}
