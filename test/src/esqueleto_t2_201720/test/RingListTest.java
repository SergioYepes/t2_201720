package esqueleto_t2_201720.test;
import junit.framework.TestCase;

import model.data_structures.RingList;
import model.data_structures.Node;
public class RingListTest extends TestCase{
	//ATTRIBUTES
	RingList<Integer> ringListTest; 
	Node<Integer> nodeFirst;
	Node<Integer> nodeLast;
	
	//CONSTRUCTOR
	private void setupEscenario1() {
		try {
			ringListTest = new RingList<>();
		}
		catch(Exception e) {
			fail("No debi� generar error al crear la lista ");
		}
		
	}
	
	private void setupEscenario2() {
		try {
			ringListTest = new RingList<>();
			nodeLast = new Node<>(new Integer(1));
			ringListTest.addNodeBeginning(nodeLast);
			Node<Integer> node = new Node<>(new Integer(1));
			ringListTest.addNodeBeginning(node);
			nodeFirst = new Node<>(new Integer(1));
			ringListTest.addNodeBeginning(nodeFirst);
		}
		catch(Exception e) {
			fail("No debi� generar error al crear la lista");
		}
	}
	
	public void testRingList() {
		setupEscenario1();
		assertEquals("Al inicio, no debe tener ning�n elemento",null,ringListTest.getElement(0));
	}
	
	public void testGetSize() {
		setupEscenario2();
		assertEquals("Debe tener tres nodos",new Integer(3),ringListTest.getSize());
	}
	
	public void testAddNodeKnown() {
		setupEscenario2();
		Node<Integer> node = new Node<>(new Integer(1));
		Node<Integer> node1 = new Node<>(new Integer(1));
		ringListTest.addNodeKnown(node, 0);
		ringListTest.addNodeKnown(node1, 3);
		assertEquals("El siguiente nodo de 'node' deber�a ser 'nodeLast'",nodeFirst,node.getNext());
		assertEquals("El siguiente nodo de 'node' deber�a ser 'nodeLast'",nodeLast,node1.getPrior());

	}
	
	public void testAddNodeBeginning() {
		setupEscenario2();
		Node<Integer> node = new Node<>(new Integer(1));
		ringListTest.addNodeBeginning(node);
		assertEquals("El nodo anterior de 'node' deber�a ser 'nodeLast'",nodeLast,node.getPrior());
		assertEquals("El siguiente nodo de 'node' deber�a ser 'nodeFirst'",nodeFirst,node.getNext());
	}
	
	public void testAddNodeEnd() {
		setupEscenario2();
		Node<Integer> node = new Node<>(new Integer(1));
		ringListTest.addNodeEnd(node);
		assertEquals("El siguiente nodo de 'node' deber�a ser 'nodeFirst'",nodeFirst, node.getNext());
		assertEquals("El nodo anterior de 'node' deber�a ser 'nodeLast'",nodeLast,node.getPrior());
	}
	
	public void testDeleteNodeKnown() {
		setupEscenario2();
		Integer size = ringListTest.getSize();
		ringListTest.deleteNodeKnown(nodeFirst);
		assertEquals("El tama�o de la lista deber�a haber desminu�do",new Integer(size-1),ringListTest.getSize());
	}
	
	public void testGetElement() {
		setupEscenario2();
		assertEquals("El �ltimo nodo debe ser nodeLast", nodeLast,ringListTest.getElement(2));
		assertEquals("El primer elemento debe ser 'nodeFirst'",nodeFirst,ringListTest.getElement(3));
		assertEquals("El primer elemento debe ser 'nodeFirst'",nodeFirst,ringListTest.getElement(0));
		assertEquals("El primer elemento debe ser 'nodeFirst'",nodeFirst.getNext(),ringListTest.getElement(1));
	}
}
